import { Component, OnInit } from '@angular/core';
import { DataService } from './checklist-viewer/providers/data/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'app';
  constructor(private dataService: DataService) {
    window['dataService'] = dataService;
  }
  ngOnInit() {
    this.dataService.setChecklist({
      pages: [
        {
          isCollapsed: false,
          pictures: [],
          title: 'Seite 1',
          showMeta: false,
          checkpoints: [
            {
              type: 'number',
              text: '<p>Wie viele Reifen sind am Fahrzeug?</p>',
              isCollapsed: false,
              answer_blocks: [],
              pictures: [
                {
                  url:
                    'pictures%2Fcheckpoint%2F5d41eba0335811e8%2F1522755035806_DSC_4145.jpg',
                  name: '1522755035806_DSC_4145.jpg',
                },
              ],
              conditionOptions: [],
              conditions: [],
              order: -1,
              mandatory: true,
              meta: [],
              enable_comment: true,
            },
          ],
          order: -1,
          showPictures: false,
          meta: [],
        },
        {
          isCollapsed: false,
          pictures: [],
          title: 'Seite 2',
          showMeta: false,
          checkpoints: [
            {
              conditionOptions: [
                {
                  value: 'Option 1',
                  label: 'Option 1',
                },
                {
                  value: 'Option 2',
                  label: 'Option 2',
                },
              ],
              conditions: [],
              order: -1,
              mandatory: true,
              meta: [],
              enable_comment: true,
              type: 'multi',
              text: '<p>Welche Option?</p>',
              isCollapsed: false,
              answer_blocks: [
                {
                  text: 'Option 1',
                },
                {
                  text: 'Option 2',
                },
              ],
              pictures: [],
            },
          ],
          order: -1,
          showPictures: false,
          meta: [],
        },
      ],
      meta: [],
      name: 'Das ist eine Test Checkliste',
      edit: {
        email: 'tico@mail.de',
        date: 1522755046778,
      },
      sys_meta: [],
      pictures: [],
      id: '5d41eba0335811e8',
    });
  }
}
