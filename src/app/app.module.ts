import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModules } from 'ng-mdb-pro';
import { MDBSpinningPreloader } from 'ng-mdb-pro';
import { AppComponent } from './app.component';
import {ChecklistViewerModule} from './checklist-viewer/checklist-viewer.module';
import { RouterModule, Routes } from '@angular/router';
import {ChecklistComponent} from './checklist-viewer/pages/checklist/checklist/checklist.component';
import { APP_BASE_HREF, Location } from '@angular/common';
import { getBaseLocation } from './shared/common-functions.util';

/*const appRoutes: Routes = [
  {
    path: 'preview/id/:page',
    component: ChecklistComponent,
  },
];*/

@NgModule({
  declarations: [AppComponent],
  imports: [
    /*    RouterModule.forRoot(
      appRoutes),*/
    BrowserModule,
    MDBBootstrapModules.forRoot(),
    ChecklistViewerModule.forRoot(),
  ],
  providers: [
    MDBSpinningPreloader,
    /*    {
      provide: APP_BASE_HREF,
      useFactory: getBaseLocation
    }*/
  ],
  schemas: [NO_ERRORS_SCHEMA],
  bootstrap: [AppComponent],
})
export class AppModule {}
