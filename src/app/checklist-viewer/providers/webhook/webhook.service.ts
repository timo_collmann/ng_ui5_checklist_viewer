import { Injectable } from '@angular/core';
import { HttpProvider } from './../../providers/http/http';
import { environment } from '../../../../environments/environment';

@Injectable()
/**
 * Webhook Service
 */
export class WebhookService {
  constructor(private http: HttpProvider) { }
  /**
   * Executes a get method for a webhook
   *
   * @returns {Promise<any>}
   */
  public getData(url: string, user?: string, password?: string): Promise<any> {
    return new Promise((resolve: any, reject: any) => {
      this.http.get(url).subscribe(
        data => {
          resolve(data['data']);
        },
        err => {
          console.log(err);
          reject(err);
        },
      );
    });
  }

  /**
   * executes a post method
   *
   * @returns {Promise<any>}
   * @param obj
   */
  public postData(
    url: string,
    obj: any,
    user?: string,
    password?: string,
  ): Promise<any> {
    return new Promise((resolve: any, reject: any) => {
      this.http.post(url, obj).subscribe(
        data => {
          resolve(data['data']);
        },
        err => {
          console.log(err);
          reject(err);
        },
      );
    });
  }

  /**
   * executes a put method
   *
   * @returns {Promise<any>}
   * @param obj
   */
  public putData(
    url: string,
    obj: any,
    user?: string,
    password?: string,
  ): Promise<any> {
    return new Promise((resolve: any, reject: any) => {
      this.http.put(url, obj).subscribe(
        data => {
          resolve(data['data']);
        },
        err => {
          console.log(err);
          reject(err);
        },
      );
    });
  }

  /**
   * Executes a get method for a webhook
   *
   * @returns {Promise<any>}
   */
  public deleteData(url: string, user?: string, password?: string): Promise<any> {
    return new Promise((resolve: any, reject: any) => {
      this.http.delete(url).subscribe(
        data => {
          resolve(data['data']);
        },
        err => {
          console.log(err);
          reject(err);
        },
      );
    });
  }
}
