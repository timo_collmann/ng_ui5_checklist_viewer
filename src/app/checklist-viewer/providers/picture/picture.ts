import { Injectable } from '@angular/core';
import { HttpProvider } from './../../providers/http/http';

import { reject } from 'q';
import { environment } from '../../../../environments/environment';
import { Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';
import { DataService } from '../data/data.service';

@Injectable()
export class PictureProvider {
  constructor(
    private http: HttpProvider,
    private domSanitizer: DomSanitizer,
    private dataService: DataService,
  ) { }

  /*  deletePicture(filename: string) {
    const url = environment.apiUrl + `/private/picture/${filename}`;
    return new Promise((resolve: any, reject: any) => {
      this.http.delete(url).subscribe(
        data => {
          const body: any = data.json();
          if (body.success && body.success === true) {
            console.log('success: ' + url);
            resolve(body.checklists);
          } else {
            reject(body);
          }
        },
        err => {
          console.log(err);
          reject(err);
        },
      );
    });
  }*/

  /**
   *
   * @param link der Link der beim Abspeichern in der Middleware zurück gegeben wird.
   *
   * Gibt aktuell einfach den body vom GET-Aufruf zurück, was dem Bild entspricht.
   * Muss vermutlich über data.blob abeholt werden um in die website einzufügen.
   * Vielleicht hilft das:
   * https://stackoverflow.com/questions/36152917/get-image-or-byte-data-with-http/40862839
   */
  public getPicture(link: any): Promise<any> {
    let url;
    if (environment.isGenerator) {
      url = environment.apiUrl + '/private/picture/' + link;
    } else {
      url = environment.apiUrl + '/viewer/picture/' + link;
    }
    return new Promise((resolve: any, reject: any) => {
      let requestOptions;
      if (environment.isGenerator) {
        requestOptions = new RequestOptions({
          responseType: ResponseContentType.Blob,
          headers: new Headers({
            // 'Content-Type': 'application/x-www-form-urlencoded',
            Authorization: localStorage.getItem('token'),
          }),
        });
      } else {
        requestOptions = new RequestOptions({
          responseType: ResponseContentType.Blob,
          headers: new Headers({
            // 'Content-Type': 'application/x-www-form-urlencoded',
            'Api-Key': this.dataService.apiKey,
          }),
        });
      }
      this.http.getBlob(url).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.log(err);
          reject(err);
        },
      );
    });

    // return this.http.getRequest(url, null);
  }

  /*  public getPicture(type: string, object_id: string): Promise<any> {
    const url =
      environment.apiUrl + '/private/picture?type=' + type + '&object_id=' + object_id;
    return new Promise((resolve: any, reject: any) => {
      this.http.get(url).subscribe(
        data => {
          const body: any = data.json();
          if (body.success && body.success === true) {
            console.log('success: ' + url);
            resolve(body.image_links);
          } else {
            reject(body);
          }
        },
        err => {
          console.log(err);
          reject(err);
        },
      );
    });
  }*/
  // TODO: POST-Picture multi-part/form-body zum hochladen eines Bildes.
  /**
   * JAVA: mit org.apache.httpcomponents -> httpclient && httpmime 4.5.3
   *
   private static void uploadFile(String cookie, String serverPath, String filepath, String uploadName, String mimetype) {
		try {
			CloseableHttpClient httpClient = HttpClients.createDefault();
			HttpPost uploadFile = new HttpPost(ENV.url() + serverPath);
			uploadFile.addHeader("cookie", cookie);
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
//			builder.addTextBody("field1", "yes", ContentType.TEXT_PLAIN);

			// This attaches the file to the POST:
			File f = new File(filepath);
			builder.addBinaryBody(
					uploadName,
			    new FileInputStream(f),
			    ContentType.create(mimetype),
			    f.getName()
			);
			HttpEntity multipart = builder.build();
			uploadFile.setEntity(multipart);
			CloseableHttpResponse response = httpClient.execute(uploadFile);
			HttpEntity responseEntity = response.getEntity();
			BufferedReader reader = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
			String line = null;
			String responseText = "";
			while((line = reader.readLine()) != null) {
				responseText += line;
			}
			System.out.println(responseText);
		}catch(IOException e) {
			e.printStackTrace();
		}
   */
}
