import { Injectable } from '@angular/core';
import { Checklist } from '../../model/checklist';

@Injectable()
export class DataService {
  filledChecklist: any;
  page;
  pageIndex;
  checkpointCounter;
  filledChecklistLength;
  apiKey = 'e319d13401e08bac52e871627a960961';
  validCheckpoints = [];
  constructor() {}

  /**
   * Sets the checklist which is displayed by the viewer
   * @param checklist
   */
  setChecklist(checklist: any) {
    this.filledChecklist = checklist;
    this.page = this.filledChecklist.pages[0];
    this.pageIndex = 0;
    this.checkpointCounter = this.filledChecklist.pages.length;
    this.filledChecklistLength = 0;
    this.filledChecklist.pages.forEach(page => {
      return (this.filledChecklistLength += page.checkpoints.length);
    });
  }

  /**
   * Sets page which is displayed by the viewer
   *
   * @param index
   */
  setPage(index) {
    this.page = this.filledChecklist.pages[index];
  }

  /**
   * Gets the checklist
   * @returns {Promise<any>}
   */
  getChecklist() {
    return new Promise((resolve: any, reject: any) => {
      resolve(this.filledChecklist);
    });
  }

  /**
   * Counts the checkpoints on all pages
   *
   * @param checkpointCounter
   */
  setCheckpointCounter(checkpointCounter) {
    this.checkpointCounter = checkpointCounter;
  }

  /**
   * Adds a valid checkpint to the valid checkpoits array
   * @param pageIndex
   * @param number
   */
  addValidCheckpoints(pageIndex, number) {
    this.validCheckpoints.splice(pageIndex, 1, number);
  }

  /**
   * Return the sum of the valid checkpoints
   *
   * @returns {number}
   */
  getValidCheckpoints(): number {
    if (this.validCheckpoints.length > 0) {
      return this.validCheckpoints.reduce(
        (accumulator, currentValue) => accumulator + currentValue,
      );
    } else {
      return 0;
    }
  }

  isChecklistValid(): boolean {
    if (this.getValidCheckpoints() === this.checkpointCounter) {
      return false;
    } else {
      return true;
    }
  }
}
