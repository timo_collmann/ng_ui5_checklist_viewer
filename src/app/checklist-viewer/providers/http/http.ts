import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';

/*
  Generated class for the HttpProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class HttpProvider {

  constructor(public httpClient: HttpClient) {
  }

  get(url: string) {
    return this.httpClient.get(url);
  }

  getBlob(url: string) {
    return this.httpClient.get(url, { responseType: 'blob' });
  }

  post(url: string, body: any) {
    return this.httpClient.post(url, body);
  }

  put(url: string, body: any) {
    return this.httpClient.put(url, body);
  }

  delete(url: string) {
    return this.httpClient.delete(url);
  }

  getRequest(url: string, returnName: string): Promise<any> {
    return new Promise((resolve: any, reject: any) => {
      this.httpClient.get(url).subscribe(
        data => {
          this.resolveResponse(url, data, returnName, resolve, reject);
        },
        err => {
          console.log(err);
          reject(err);
        },
      );
    });
  }

  postRequest(url: string, postBody: any, returnName: string): Promise<any> {
    return new Promise((resolve: any, reject: any) => {
      if (postBody != null) {
        this.httpClient
          .post(url, {
            postBody,
          })
          .subscribe(
            data => {
              this.resolveResponse(url, data, returnName, resolve, reject);
            },
            err => {
              console.log(err);
              reject(err);
            },
        );
      } else {
        reject('Nobody to post');
      }
    });
  }

  deleteRequest(url: string, returnName: string): Promise<any> {
    return new Promise((resolve: any, reject: any) => {
      this.httpClient.delete(url).subscribe(
        data => {
          this.resolveResponse(url, data, returnName, resolve, reject);
        },
        err => {
          console.log(err);
          reject(err);
        },
      );
    });
  }

  private resolveResponse(
    url: string,
    data: any,
    returnName: string,
    resolve: any,
    reject: any,
  ) {
    console.log(data);
    const body: any = data.json();
    if (body.success && body.success === true) {
      console.log('success: ' + url);
      if (returnName != null) {
        resolve(body[returnName]);
      } else {
        resolve(body);
      }
    } else {
      reject(body);
    }
  }
}
