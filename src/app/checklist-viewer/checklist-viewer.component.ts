import { Component, ViewChild, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './checklist-viewer.component.html',
  styleUrls: ['./checklist-viewer.component.scss'],
})
export class AppComponent {
  title = 'app';
  panelOpenState: boolean = false;
}
