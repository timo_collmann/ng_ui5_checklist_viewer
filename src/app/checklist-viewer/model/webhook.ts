export class Webhook {
  id: string;
  name: string;
  type: string;
  description: string;
  url: string;
  proxy: string;
  user: string;
  password: string;
}
