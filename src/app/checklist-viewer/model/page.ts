import {Checkpoint} from './checkpoint';
import {Meta} from './meta';
import {Picture} from './picture';

export class Page {
  title: string = '';
  text: string;
  order: number = -1;
  checkpoints: Checkpoint[] = [];
  meta: Meta[] = [];
  pictures: Picture[] = [];
  isCollapsed = false;
  showMeta = false;
  showPictures = false;
}
