import {AnswerBlock} from './answerblock';
import {Meta} from './meta';
import {Condition} from './condition';
import {Picture} from './picture';
import {Answer} from './answer';

export class Checkpoint {
  id: string;
  text: string;
  type: string;
  order: number = -1;
  value: any;
  style: string;
  mandatory: boolean = true;
  answer_blocks: AnswerBlock[] = [];
  meta: Meta[] = [];
  conditions: Condition[] = [];
  pictures: Picture[] = [];
  isCollapsed= false;
  enable_comment = true;
  answer: Answer;
}
