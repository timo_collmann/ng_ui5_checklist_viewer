import { Meta } from './meta';
import { Page } from './page';
import { Picture } from './picture';
export class Checklist {
  version: number;
  id: string;
  name: string;
  meta: Meta[] = [];
  sys_meta: Meta[] = [];
  pages: Page[] = [];
  pictures: Picture[] = [];
}
