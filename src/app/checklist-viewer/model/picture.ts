import {SafeUrl} from "@angular/platform-browser";

export class Picture {
  url: string;
  name: string;
  blobUrl?: SafeUrl;
}
