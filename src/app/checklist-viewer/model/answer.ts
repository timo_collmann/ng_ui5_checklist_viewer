export class Answer {
  value: any[];
  order: number = -1;
  comment: string;
  picture: boolean = false;
}
