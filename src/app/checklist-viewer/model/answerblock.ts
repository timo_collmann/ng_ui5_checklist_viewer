import { Answer } from './answer';
export class AnswerBlock {
  text: string;
  min: number = 1;
  max: number = 1;
  order: number = -1;
  answers: Answer[] = [];
}
