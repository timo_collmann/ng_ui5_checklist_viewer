export class Condition {
  name: string;
  operator: string;
  action: string;
  compare: string;
}
