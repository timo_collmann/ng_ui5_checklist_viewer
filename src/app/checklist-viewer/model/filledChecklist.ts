import { Meta } from './meta';
import { Page } from './page';
import { Picture } from './picture';
import {Answer} from './answer';
export class FilledChecklist {
  version: number;
  id: string;
  name: string;
  meta: Meta[] = [];
  sys_meta: Meta[] = [];
  pages: Page[] = [];
  pictures: Picture[] = [];
  progress: number = 0;
}
