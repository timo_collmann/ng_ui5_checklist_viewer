import { NgModule } from '@angular/core';
import { ChecklistComponent } from './pages/checklist/checklist/checklist.component';
import { RouterModule } from '@angular/router';

const checklistViewerRoutes = [
  {
    path: 'checklist2/:id/:page',
    component: ChecklistComponent
  },
];
@NgModule({
  imports: [RouterModule.forChild(checklistViewerRoutes)],
  exports: [RouterModule]
})
export class ChecklistViewerRoutingModule {}
