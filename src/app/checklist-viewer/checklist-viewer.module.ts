import { ModuleWithProviders, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MDBBootstrapModules } from 'ng-mdb-pro';
import { MDBSpinningPreloader } from 'ng-mdb-pro';
import { AppComponent } from './checklist-viewer.component';
import { ChecklistComponent } from './pages/checklist/checklist/checklist.component';
import { PageComponent } from './pages/checklist/checklist/page/page.component';
import { PictureComponent } from './pages/checklist/checklist/page/picture/picture.component';
import { QuillEditorModule } from 'ngx-quill-editor';
import { CommentComponent } from './pages/checklist/checklist/page/comment/comment.component';
import { CommonModule } from '@angular/common';
import { ChecklistViewerRoutingModule } from './checklist-viewer-routing.module';
import { HttpProvider } from './providers/http/http';
import { DataService } from './providers/data/data.service';
import { DateComponent } from './pages/checklist/checklist/page/checkpointTypes/date/date.component';
import { DropdownComponent } from './pages/checklist/checklist/page/checkpointTypes/dropdown/dropdown.component';
import { MultiplechoiceComponent } from './pages/checklist/checklist/page/checkpointTypes/multiplechoice/multiplechoice.component';
import { NumberComponent } from './pages/checklist/checklist/page/checkpointTypes/number/number.component';
import { TextComponent } from './pages/checklist/checklist/page/checkpointTypes/text/text.component';
import { TimeComponent } from './pages/checklist/checklist/page/checkpointTypes/time/time.component';
import { RadioComponent } from './pages/checklist/checklist/page/checkpointTypes/radio/radio.component';
import { LtextComponent } from './pages/checklist/checklist/page/checkpointTypes/ltext/ltext.component';
import { WebhookService } from './providers/webhook/webhook.service';
import { PictureProvider } from './providers/picture/picture';
import { CameraPictureComponent } from './pages/checklist/checklist/page/checkpointTypes/camera-picture/camera-picture.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TokenInterceptor } from './providers/token-interceptor/token-interceptor.service';
import { CameraPictureRequiredDirective } from './pages/checklist/checklist/page/checkpointTypes/camera-picture/camera-picture-required.directive';
import { LtextRequiredDirective } from './pages/checklist/checklist/page/checkpointTypes/ltext/ltext-required.directive';
import { TimeRequiredDirective } from './pages/checklist/checklist/page/checkpointTypes/time/time-required.directive';
import { MultiplechoiceRequiredDirective } from './pages/checklist/checklist/page/checkpointTypes/multiplechoice/multiplechoice-required.directive';

@NgModule({
  declarations: [
    AppComponent,
    ChecklistComponent,
    DateComponent,
    DropdownComponent,
    MultiplechoiceComponent,
    NumberComponent,
    TextComponent,
    TimeComponent,
    PageComponent,
    RadioComponent,
    PictureComponent,
    CommentComponent,
    LtextComponent,
    CameraPictureComponent,
    CameraPictureRequiredDirective,
    TimeRequiredDirective,
    LtextRequiredDirective,
    MultiplechoiceRequiredDirective,
  ],
  imports: [
    CommonModule,
    MDBBootstrapModules.forRoot(),
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    QuillEditorModule,
    ChecklistViewerRoutingModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ],
  exports: [AppComponent, ChecklistComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class ChecklistViewerModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ChecklistViewerModule,
      providers: [
        MDBSpinningPreloader,
        HttpProvider,
        DataService,
        WebhookService,
        PictureProvider,
      ],
    };
  }
}
