import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { NgForm } from '@angular/forms';
import { Checklist } from '../../../model/checklist';
import { DataService } from '../../../providers/data/data.service';
import { WebhookService } from '../../../providers/webhook/webhook.service';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.scss'],
})
export class ChecklistComponent implements OnInit, AfterViewInit {
  filledChecklist: any;
  loading = false;
  checkpointCounter = 0;
  @ViewChild('form') form: NgForm;
  validCounter;
  runners;
  isChecklistValid: boolean;

  constructor(
    public dataService: DataService,
    private webhookService: WebhookService,
  ) {}

  ngOnInit() {
    this.dataService.getChecklist().then(filledChecklist => {
      this.filledChecklist = filledChecklist;
      this.filledChecklist['pages'].forEach(page => {
        page['checkpoints'].forEach(() => {
          this.checkpointCounter++;
        });
      });
      this.dataService.setCheckpointCounter(this.checkpointCounter);
      this.dataService.getValidCheckpoints();
    });
  }
  ngAfterViewInit() {
    for (const control in this.form.form.controls) {
      if (this.form.form.controls[control].status === 'VALID') {
        this.validCounter++;
      }
    }
    this.dataService.addValidCheckpoints(
      this.dataService.pageIndex,
      this.validCounter,
    );
    this.isChecklistValid = this.dataService.isChecklistValid();
    this.form.form.valueChanges.subscribe(data => {
      this.validCounter = 0;
      for (const control in this.form.form.controls) {
        if (this.form.form.controls[control].status === 'VALID') {
          this.validCounter++;
        }
        if (this.form.form.controls[control].status === 'INVALID') {
        }
      }
      this.dataService.addValidCheckpoints(
        this.dataService.pageIndex,
        this.validCounter,
      );
      this.dataService.getValidCheckpoints();
      this.isChecklistValid = this.dataService.isChecklistValid();
    });
  }

  decreasePageIndex() {
    this.dataService.addValidCheckpoints(
      this.dataService.pageIndex,
      this.validCounter,
    );
    this.validCounter = 0;
    this.dataService.setPage(--this.dataService.pageIndex);
    this.dataService.getValidCheckpoints();
  }

  increasePageIndex() {
    this.dataService.addValidCheckpoints(
      this.dataService.pageIndex,
      this.validCounter,
    );
    this.validCounter = 0;
    this.dataService.setPage(++this.dataService.pageIndex);
    this.dataService.getValidCheckpoints();
  }

  storeFilledChecklistinService() {
    this.dataService.setChecklist(this.filledChecklist);
  }

  /**
   * Saves the checklist
   */
  saveFilledChecklist() {
    console.log(
      'Die ausgefüllte Checkliste: ' + JSON.stringify(this.filledChecklist),
    );
    console.log(this.runners);
    console.log(
      `Groesse des JSON: ${JSON.stringify(this.filledChecklist).length * 8}`,
    );
  }

  /**
   * Syncs the checklist and runs the webhooks
   */
  // TODO
  syncAndRunWebhooks() {
    this.runners = [];
    this.filledChecklist.pages.forEach(page => {
      page.checkpoints.forEach(checkpoint => {
        const runner = { answer: {}, conditions: [] };
        runner.answer = checkpoint.answer;
        checkpoint.conditions.forEach(condition => {
          console.log(condition);

          runner.conditions.push(condition);
        });
        this.runners.push(runner);
      });
    });
    console.log('Sync');
    console.log(this.runners);
    this.runners.forEach(runner => {
      runner.conditions.forEach(condition => {
        // TODO Different kinds af answer values
        if (
          (runner.answer.value.constructor !== Array &&
            this.compareLiteral(
              runner.answer.value,
              condition.operator,
              condition.compare,
            )) ||
          (runner.answer.value.constructor === Array &&
            this.compareMultiPartAnswer(
              runner.answer.value,
              condition.operator,
              condition.compare,
            ))
        ) {
          if (!condition.urlParameter) {
            switch (condition.webhook.type) {
              // TODO Other http methods
              case 'GET':
                this.webhookService
                  .getData(condition.webhook.url)
                  .then(response => {
                    console.log(response);
                  })
                  .catch(error => {
                    console.log(error);
                  });
                break;
              case 'POST':
                const postObject = {};
                condition.webhookParameters.forEach(webhookParameter => {
                  postObject[webhookParameter.name] =
                    webhookParameter.parameter;
                });
                this.webhookService
                  .postData(`${condition.webhook.url}`, postObject)
                  .then(response => {
                    console.log(response);
                  })
                  .catch(error => {
                    console.log(error);
                  });
                break;
              case 'PUT':
                const putObject = {};
                condition.webhookParameters.forEach(webhookParameter => {
                  putObject[webhookParameter.name] = webhookParameter.parameter;
                });
                this.webhookService
                  .putData(`${condition.webhook.url}`, putObject)
                  .then(response => {
                    console.log(response);
                  })
                  .catch(error => {
                    console.log(error);
                  });
                break;
              case 'DELETE':
                this.webhookService
                  .deleteData(`${condition.webhook.url}`)
                  .then(response => {
                    console.log(response);
                  })
                  .catch(error => {
                    console.log(error);
                  });
                break;
              default:
                break;
            }
          } else {
            switch (condition.webhook.type) {
              // TODO Other http methods
              case 'GET':
                this.webhookService
                  .getData(`${condition.webhook.url}/${condition.urlParameter}`)
                  .then(response => {
                    console.log(response);
                  })
                  .catch(error => {
                    console.log(error);
                  });
                break;
              case 'POST':
                const postObject = {};
                condition.webhookParameters.forEach(webhookParameter => {
                  postObject[webhookParameter.name] =
                    webhookParameter.parameter;
                });
                this.webhookService
                  .postData(
                    `${condition.webhook.url}/${condition.urlParameter}`,
                    postObject,
                  )
                  .then(response => {
                    console.log(response);
                  })
                  .catch(error => {
                    console.log(error);
                  });
                break;
              case 'PUT':
                const putObject = {};
                condition.webhookParameters.forEach(webhookParameter => {
                  putObject[webhookParameter.name] = webhookParameter.parameter;
                });
                this.webhookService
                  .putData(
                    `${condition.webhook.url}/${condition.urlParameter}`,
                    putObject,
                  )
                  .then(response => {
                    console.log(response);
                  })
                  .catch(error => {
                    console.log(error);
                  });
                break;
              case 'DELETE':
                this.webhookService
                  .deleteData(
                    `${condition.webhook.url}/${condition.urlParameter}`,
                  )
                  .then(response => {
                    console.log(response);
                  })
                  .catch(error => {
                    console.log(error);
                  });
                break;
              default:
                break;
            }
          }
        }
      });
    });
  }

  /**
   * Compares answer and condition
   *
   * @param answer
   * @param operator
   * @param value
   * @returns {boolean}
   */
  compareLiteral(answer, operator, value) {
    debugger;
    switch (operator) {
      case '>':
        return answer > value;
      case '<':
        return answer < value;
      case '>=':
        return answer >= value;
      case '<=':
        return answer <= value;
      case '==':
        return answer == value;
      case '!=':
        return answer != value;
      case '===':
        return answer === value;
      case '!==':
        return answer !== value;
    }
  }

  compareMultiPartAnswer(answer: Array<any>, operator: string, value) {
    debugger;
    switch (operator) {
      case '==':
        return answer.indexOf(value) > -1;
      case '!=':
        return answer.indexOf(value) === -1;
    }
  }
}
