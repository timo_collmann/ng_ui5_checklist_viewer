import { Component, Input, OnInit } from '@angular/core';
import { Checkpoint } from '../../../../../model/checkpoint';
import { FilledChecklist } from '../../../../../model/filledChecklist';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
})
export class CommentComponent implements OnInit {
  @Input() source: Checkpoint;
  editorOptions = {
    modules: {
      toolbar: [[{ list: 'ordered' }, { list: 'bullet' }]],
    },
    placeholder: 'Kommentar',
  };
  constructor() {}

  ngOnInit() {}
}
