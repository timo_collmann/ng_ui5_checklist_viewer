import { Component, Input, OnInit } from "@angular/core";
import { PictureProvider } from "../../../../../providers/picture/picture";
import { DomSanitizer } from "@angular/platform-browser";
import { Picture } from "../../../../../model/picture";

@Component({
  selector: "app-picture",
  templateUrl: "./picture.component.html",
  styleUrls: ["./picture.component.scss"]
})
export class PictureComponent implements OnInit {
  @Input() source;
  pictures = [];
  constructor(
    private pictureProvider: PictureProvider,
    private domSanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.source.pictures.forEach(picture => {
      if (picture) {
        this.pictureProvider.getPicture(picture.url).then(response => {
          const pic = new Picture();
          pic.blobUrl = this.domSanitizer.bypassSecurityTrustUrl(
            URL.createObjectURL(response.blob())
          );
          pic.name = picture.name;
          pic.url = picture.url;
          this.pictures.push(pic);
        });
      }
    });
  }
}
