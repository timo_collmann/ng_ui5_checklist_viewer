import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl } from '@angular/forms';

@Directive({
  selector: 'app-camera-picture[required]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: CameraPictureRequiredDirective, multi: true }
  ]
})
export class CameraPictureRequiredDirective implements Validator {

  validate(c: FormControl) {
    if (c.value === false) {
      return {
        'required': 'Please upload a picture'
      };
    }

    return null;
  }

}
