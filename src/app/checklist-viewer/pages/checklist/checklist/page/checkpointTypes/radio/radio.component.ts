import { Component, Input, OnInit } from '@angular/core';
import { Checkpoint } from '../../../../../../model/checkpoint';
import { Answer } from '../../../../../../model/answer';
import { ControlContainer, NgForm } from '@angular/forms';

@Component({
  selector: 'app-checkpoint',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss'],
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm }],
})
export class RadioComponent implements OnInit {
  @Input() checkpoint: Checkpoint;
  @Input() checkpointIndex;
  constructor() {}

  ngOnInit() {
    if (!this.checkpoint.answer) {
      this.checkpoint.answer = new Answer();
    }
  }
  changeSelectedValue(value) {
    this.checkpoint.answer.value = value;
  }
}
