import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { Answer } from '../../../../../../model/answer';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TimeComponent } from '../time/time.component';

@Component({
  selector: 'app-ltext',
  templateUrl: './ltext.component.html',
  styleUrls: ['./ltext.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => LtextComponent),
      multi: true,
    },
  ],
})
export class LtextComponent implements OnInit, ControlValueAccessor {
  @Input() checkpoint;
  @Input() checkpointIndex;
  propagateChange = (_: any) => {};
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {}

  setDisabledState(isDisabled: boolean): void {}

  writeValue(obj: any): void {}

  constructor() {}

  ngOnInit() {
    if (!this.checkpoint.answer) {
      this.checkpoint.answer = new Answer();
    }
  }
  validateForm() {
    if (this.checkpoint.answer.value !== '') {
      this.propagateChange(this.checkpoint.answer.value);
    } else {
      this.propagateChange(null);
    }
  }
}
