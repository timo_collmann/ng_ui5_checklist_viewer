import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { Checkpoint } from '../../../../../../model/checkpoint';
import { Answer } from '../../../../../../model/answer';
import {
  ControlContainer,
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
  NgForm,
} from '@angular/forms';

@Component({
  selector: 'app-multiplechoice',
  templateUrl: './multiplechoice.component.html',
  styleUrls: ['./multiplechoice.component.scss'],
  //viewProviders: [{ provide: ControlContainer, useExisting: NgForm }],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultiplechoiceComponent),
      multi: true,
    },
  ],
})
export class MultiplechoiceComponent implements OnInit, ControlValueAccessor {
  @Input() checkpoint: Checkpoint;
  @Input() checkpointIndex;
  propagateChange = (_: any) => {};

  constructor() {}

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {}

  setDisabledState(isDisabled: boolean): void {}

  writeValue(obj: any): void {}

  ngOnInit() {
    if (!this.checkpoint.answer) {
      this.checkpoint.answer = new Answer();
    }
    this.checkpoint.answer.value = [];
  }
  onSelected(item, event) {
    if (event.target.checked) {
      this.checkpoint.answer.value.push(item);
    } else {
      const index = this.checkpoint.answer.value.indexOf(item);
      this.checkpoint.answer.value.splice(index, 1);
    }
  }
  validateForm() {
    if (this.checkpoint.answer.value.length > 0) {
      this.propagateChange(this.checkpoint.answer.value);
    } else {
      this.propagateChange([]);
    }
  }
}
