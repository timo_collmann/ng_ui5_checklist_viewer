import { Component, Input, OnInit } from '@angular/core';
import { Answer } from '../../../../../../model/answer';
import { ControlContainer, NgForm } from '@angular/forms';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss'],
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm }],
})
export class TextComponent implements OnInit {
  @Input() checkpoint;
  @Input() checkpointIndex;

  constructor() {}

  ngOnInit() {
    if (!this.checkpoint.answer) {
      this.checkpoint.answer = new Answer();
    }
  }
}
