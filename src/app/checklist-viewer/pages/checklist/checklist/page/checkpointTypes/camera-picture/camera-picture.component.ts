import {
  AfterViewInit,
  Component,
  ElementRef,
  forwardRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Answer } from '../../../../../../model/answer';

import { fabric } from 'fabric';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-camera-picture',
  templateUrl: './camera-picture.component.html',
  styleUrls: ['./camera-picture.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CameraPictureComponent),
      multi: true,
    },
  ],
})
export class CameraPictureComponent
  implements OnInit, AfterViewInit, ControlValueAccessor {
  @Input() checkpoint;
  @Input() checkpointIndex;
  loading = false;
  @ViewChild('div') div: ElementRef;
  canvas;
  imageObj: HTMLImageElement;
  url: string;
  isPictureLoaded;
  editMode = false;
  //method to handle the status of the form control
  propagateChange = (_: any) => {};

  constructor() {}

  writeValue(value) {}

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {}
  ngOnInit() {
    if (!this.checkpoint.answer) {
      this.checkpoint.answer = new Answer();
    }
  }

  ngAfterViewInit() {
    const that = this;
    this.canvas = new fabric.Canvas('myCanvas_' + that.checkpointIndex);
    this.canvas.setDimensions({ width: 0, height: 0 });
    this.canvas.selection = false;
    this.canvas.allowTouchScrolling = true;
    this.canvas.freeDrawingBrush.color = '#ff0000';
    this.canvas.freeDrawingBrush.width = 10;
    this.canvas.drawingMode = false;
    this.imageObj = new Image();
    this.canvas.on({
      'object:added': this.modiefiedHandler.bind(this),
      'object:modified': this.modiefiedHandler.bind(this),
    });

    if (this.checkpoint.answer.value) {
      fabric.Image.fromURL(that.checkpoint.answer.value, function(img) {
        const aspectRatio = img.width / img.height;
        const width = that.div.nativeElement.offsetWidth;
        const height = that.div.nativeElement.offsetWidth / aspectRatio;
        that.canvas.drawingMode = false;
        that.canvas.setDimensions({ width: width, height: height });
        that.canvas.setBackgroundImage(
          img,
          that.canvas.renderAll.bind(that.canvas),
          {
            scaleX: that.canvas.width / img.width,
            scaleY: that.canvas.height / img.height,
          },
        );
        that.checkpoint.answer.value = that.canvas.toDataURL({});
      });
      that.isPictureLoaded = true;
    }
  }
  /**
   * Toggles the edit mode
   *
   */
  toggleEditMode() {
    if (this.editMode === true) {
      this.canvas.isDrawingMode = false;
      this.canvas.allowTouchScrolling = true;
      this.editMode = false;
    } else {
      this.canvas.isDrawingMode = true;
      this.canvas.allowTouchScrolling = false;
      this.editMode = true;
    }
  }
  /**
   * Stores the base64 encoded image in the json if the canvas is edited
   *
   * @param evt
   */
  modiefiedHandler(evt) {
    this.checkpoint.answer.value = evt.target.canvas.toDataURL({});
  }

  /**
   * Removes the canvas
   */
  clearCanvas() {
    this.canvas.clear();
    this.isPictureLoaded = false;
    this.canvas.setDimensions({ width: 0, height: 0 });
    this.canvas.drawingMode = false;
    this.url = null;
    this.checkpoint.answer.value = null;
    this.propagateChange(null);
  }

  /**
   * Removes the drawing from the canvas
   */
  removeDrawings() {
    const objects = this.canvas.getObjects();

    let index = objects.length - 1;

    while (index >= 0) {
      this.canvas.remove(objects[index]);
      index -= 1;
    }
  }

  /**
   * Handles image upload
   *
   * @param e
   */
  handleFile(e) {
    e.stopPropagation();
    e.preventDefault();
    this.url = URL.createObjectURL(e.target.files[0]);
    this.imageObj.src = this.url;
    const that = this;
    this.imageObj.onload = function() {
      fabric.Image.fromURL(that.url, function(img) {
        const aspectRatio = img.width / img.height;
        const width = that.div.nativeElement.offsetWidth;
        const height = that.div.nativeElement.offsetWidth / aspectRatio;
        that.canvas.drawingMode = false;
        that.canvas.setDimensions({ width: width, height: height });
        that.canvas.setBackgroundImage(
          img,
          that.canvas.renderAll.bind(that.canvas),
          {
            scaleX: that.canvas.width / img.width,
            scaleY: that.canvas.height / img.height,
          },
        );
        that.checkpoint.answer.value = that.canvas.toDataURL({});
      });
      that.isPictureLoaded = true;
      that.propagateChange(that.checkpoint.answer.value);
    };
  }

  /**
   * Recalculates the size of the canvas and the postion/size of the drawn elements
   *
   * @param event
   */
  onResize(event) {
    const that = this;
    const zoom = this.div.nativeElement.offsetWidth / this.canvas.width;
    if (zoom !== 1) {
      if (this.checkpoint.answer.value) {
        that.url = this.checkpoint.answer.value;
      }
      fabric.Image.fromURL(that.url, function(img) {
        const aspectRatio = img.width / img.height;
        const width = that.div.nativeElement.offsetWidth;
        const height = that.div.nativeElement.offsetWidth / aspectRatio;
        that.canvas.drawingMode = false;
        that.canvas.setDimensions({ width: width, height: height });
        that.canvas.setBackgroundImage(
          img,
          that.canvas.renderAll.bind(that.canvas),
          {
            scaleX: that.canvas.width / img.width,
            scaleY: that.canvas.height / img.height,
          },
        );
      });
      const objects = this.canvas.getObjects();
      objects.forEach(object => {
        console.log(object);
        object.scaleX = object.scaleX * zoom;
        object.scaleY = object.scaleY * zoom;
        object.left = object.left * zoom;
        object.top = object.top * zoom;
        object.setCoords();
      });
      this.canvas.renderAll();
    }
  }
}
