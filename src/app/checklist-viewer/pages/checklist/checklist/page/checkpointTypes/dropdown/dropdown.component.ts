import { Component, Input, OnInit } from '@angular/core';
import { ControlContainer, NgForm } from '@angular/forms';
import { Checkpoint } from '../../../../../../model/checkpoint';
import { Answer } from '../../../../../../model/answer';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm }],
})
export class DropdownComponent implements OnInit {
  @Input() checkpoint: Checkpoint;
  @Input() checkpointIndex;
  options: Array<any>;
  name;
  selectedValue;
  constructor() {}

  ngOnInit() {
    if (!this.checkpoint.answer) {
      this.checkpoint.answer = new Answer();
    }

    this.options = this.checkpoint.answer_blocks.map(option => {
      return { value: option.text, label: option.text };
    });
    this.name = this.checkpoint.text;
  }
}
