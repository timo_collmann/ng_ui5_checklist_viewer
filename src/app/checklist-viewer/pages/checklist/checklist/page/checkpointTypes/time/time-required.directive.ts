import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl } from '@angular/forms';

@Directive({
  selector: 'app-time[required]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: TimeRequiredDirective, multi: true }
  ]
})
export class TimeRequiredDirective implements Validator {

  validate(c: FormControl) {
    if (c.value === false) {
      return {
        'required': 'Please upload a picture'
      };
    }

    return null;
  }

}
