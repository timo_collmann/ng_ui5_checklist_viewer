import { Component, Input, OnInit } from '@angular/core';
import { Checkpoint } from '../../../../../../model/checkpoint';
import { Answer } from '../../../../../../model/answer';
import { ControlContainer, NgForm } from '@angular/forms';

@Component({
  selector: 'app-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.scss'],
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm }],
})
export class NumberComponent implements OnInit {
  @Input() checkpoint: Checkpoint;
  @Input() checkpointIndex;
  constructor() {}

  ngOnInit() {
    if (!this.checkpoint.answer) {
      this.checkpoint.answer = new Answer();
    }
  }
}
