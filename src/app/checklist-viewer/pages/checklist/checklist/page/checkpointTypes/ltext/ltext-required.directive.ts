import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl } from '@angular/forms';

@Directive({
  selector: 'app-ltext[required]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: LtextRequiredDirective, multi: true }
  ]
})
export class LtextRequiredDirective implements Validator {

  validate(c: FormControl) {
    if (c.value === false) {
      return {
        'required': 'Please upload a picture'
      };
    }

    return null;
  }

}
