import { Component, Input, OnInit } from '@angular/core';
// TODO: mdbootstrap im modul installieren und referenzieren
import { IMyOptions } from '../../../../../../../../../node_modules/ng-mdb-pro/pro/date-picker';

import { ControlContainer, NgForm } from '@angular/forms';
import {Answer} from '../../../../../../model/answer';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss'],
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm }],
})
export class DateComponent implements OnInit {
  @Input() checkpoint;
  @Input() checkpointIndex;
  public myDatePickerOptions: IMyOptions = {
    dayLabels: {
      su: 'Son',
      mo: 'Mon',
      tu: 'Die',
      we: 'Mit',
      th: 'Don',
      fr: 'Fre',
      sa: 'Sam',
    },
    dayLabelsFull: {
      su: 'Sonntag',
      mo: 'Montag',
      tu: 'Dienstag',
      we: 'Mittwoch',
      th: 'Donnerstag',
      fr: 'Freitag',
      sa: 'Samstag',
    },
    monthLabels: {
      1: 'Jan',
      2: 'Feb',
      3: 'Mär',
      4: 'Apr',
      5: 'Mai',
      6: 'Jun',
      7: 'Jul',
      8: 'Aug',
      9: 'Sep',
      10: 'Okt',
      11: 'Nov',
      12: 'Dez',
    },
    monthLabelsFull: {
      1: 'Januar',
      2: 'Februar',
      3: 'März',
      4: 'April',
      5: 'Mai',
      6: 'Juni',
      7: 'Juli',
      8: 'August',
      9: 'September',
      10: 'Oktober',
      11: 'November',
      12: 'Dezember',
    },
  };

  constructor() {}

  ngOnInit() {
    if (!this.checkpoint.answer) {
      this.checkpoint.answer = new Answer();
    }
  }
}
