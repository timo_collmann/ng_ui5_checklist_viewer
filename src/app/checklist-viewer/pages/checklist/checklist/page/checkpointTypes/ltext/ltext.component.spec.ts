import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LtextComponent } from './ltext.component';

describe('LtextComponent', () => {
  let component: LtextComponent;
  let fixture: ComponentFixture<LtextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LtextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LtextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
