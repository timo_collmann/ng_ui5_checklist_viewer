import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl } from '@angular/forms';

@Directive({
  selector: 'app-multiplechoice[required]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: MultiplechoiceRequiredDirective, multi: true }
  ]
})
export class MultiplechoiceRequiredDirective implements Validator {

  validate(c: FormControl) {
    if (c.value.length < 1) {
      return {
        'required': 'Please select at least one option'
      };
    }

    return null;
  }

}
