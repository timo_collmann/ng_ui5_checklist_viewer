import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { Answer } from '../../../../../../model/answer';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CameraPictureComponent } from '../camera-picture/camera-picture.component';

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TimeComponent),
      multi: true,
    },
  ],
})
export class TimeComponent implements OnInit, ControlValueAccessor {
  @Input() checkpoint;
  @Input() checkpointIndex;
  constructor() {}

  propagateChange = (_: any) => {};

  writeValue(obj: any): void {}

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {}

  setDisabledState(isDisabled: boolean): void {}

  ngOnInit() {
    if (!this.checkpoint.answer) {
      this.checkpoint.answer = new Answer();
    }
  }
  validateForm() {
    if (this.checkpoint.answer.value !== '') {
      this.propagateChange(this.checkpoint.answer.value);
    } else {
      this.propagateChange(null);
    }
  }
}
