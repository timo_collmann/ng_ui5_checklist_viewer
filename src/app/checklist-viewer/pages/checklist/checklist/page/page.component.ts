import {Component, Input, OnInit} from '@angular/core';
import {FilledChecklist} from '../../../../model/filledChecklist';
import {ControlContainer, NgForm} from '@angular/forms';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  viewProviders: [ { provide: ControlContainer, useExisting: NgForm } ]
})
export class PageComponent implements OnInit {
  @Input() page;
  constructor() { }

  ngOnInit() {
  }

}
