export const environment = {
  production: true,
  apiUrl: 'https://bmt-checklist-gen.appspot.com',
  isGenerator: false,
};
